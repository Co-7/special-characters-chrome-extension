$('.tab').click(function() {
    $('.tab').removeClass('active')
    $(this).addClass('active');
    var el = $(this).attr('class').split(' ')[1]
    $('.content').removeClass('active')
    $('.content.' + el).addClass('active')
})

var reset = $('#reset_button');

reset.click(function () {

    let result = confirm("Attention tous les parametres y compris les caractères ajouter vont être supprimer et reinitialisé à l'etat d'origine ! Etes vous certains ?");

    if (result) {
        chrome.storage.local.clear();
        arrayFavoris = ["À", "Á", "È", "É", "Ê", "😀","😃","😄"];
    }
});