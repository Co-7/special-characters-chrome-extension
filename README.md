# Caractères spéciaux

Une extension permettant de copier facilement les caractères spéciaux dans le presse-papiers.

Pour copier un caractère, il suffit de cliquer sur celui-ci.

Vous avez la possibilité de mettre les caractères en signet en double-cliquant dessus.

Un document résumant les fonctions possibles de l'extension est disponible dans celle-ci.